<?php

namespace App\Domain\Model;

class Product
{
    /**
     * @var string
     */
    private $id;

    /**
     * Наименование товара.
     *
     * @var string
     */
    private $title;

    /**
     * Артикул.
     *
     * @var string
     */
    private $code;

    public function __construct(string $id, string $title, string $code)
    {
        $this->id = $id;
        $this->title = $title;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
