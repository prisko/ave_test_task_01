<?php

namespace App\Application\UseCase\CreateProduct;

use App\Domain\Model\Product;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateProductHandler implements MessageHandlerInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Создает продукт по команде, возвращает ид этого продукта.
     *
     * @param CreateProduct $command
     *
     * @return string ид продукта
     */
    public function __invoke(CreateProduct $command): string
    {
    	//Проверяем, нет ли продукта с таким кодом
		$oExistsProduct = $this->productRepository->findByCode($command->getCode());
		if ($oExistsProduct) {
			return $oExistsProduct->getId();
		}
		//Если нет - создадим. Так как структура таблицы удивительна (строковый id)
		//сохраним продукт со следующий свободным id  вызовом специально созданного для этого методом
		return $this->productRepository->add($command->getTitle(), $command->getCode());
    }
}
