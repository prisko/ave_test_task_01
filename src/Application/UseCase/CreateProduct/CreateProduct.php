<?php

namespace App\Application\UseCase\CreateProduct;

use Symfony\Component\HttpFoundation\Request;

final class CreateProduct
{
    /**
     * Наименование продукта, который будет создан.
     *
     * @var string
     */
    private $title;

    /**
     * Наименование продукта, который будет создан.
     *
     * @var string
     */
    private $code;

    public function __construct(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        $this->title = $content['title'];
        $this->code = $content['code'];
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
