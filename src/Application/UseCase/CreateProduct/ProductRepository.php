<?php

namespace App\Application\UseCase\CreateProduct;

use App\Domain\Model\Product;
use App\Infrastructure\Entity\ProductPersistenceObject;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Репозиторий для доменной модели продукта
 */
class ProductRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findById(string $id): Product
    {
        /** @var ProductPersistenceObject $object */
        $object = $this->entityManager->find(ProductPersistenceObject::class, $id);

        return new Product(
            $object->getId(),
            $object->getTitle(),
            $object->getCode()
        );
    }

	public function findByCode(string $sCode): ?Product
	{
		$oQueryBuilder = $this->entityManager->createQueryBuilder();
		$oQueryBuilder->select('p.id, p.code, p.title')
			->from(ProductPersistenceObject::class, 'p' )
			->where( $oQueryBuilder->expr()->eq('p.code', ':code') )
			->setParameter(':code', $sCode)
			->setMaxResults(1);
		$array = $oQueryBuilder->getQuery()->execute();
		$array = ($array[0] ?? null);
		if ($array) {
			return new Product(
				$array['id'],
				$array['title'],
				$array['code']
			);
		}
		return null;
	}
	/**
	 * TODO добавить в таблицу целочисленный идентификатор если это согласуется с бизнес логикой
	 * Находит максимальный идентификатор в таблице product_persistence_object, приводит его к целому и возвращает значение MAX + 1
	 * @return int максимальный идентификатор + 1
	*/
	public function getNextId(): int
	{
		$oQueryBuilder = $this->entityManager->createQueryBuilder();
		$oQueryBuilder->select('MAX(p.id) AS m')
			->from(ProductPersistenceObject::class, 'p' );
		$aResult = $oQueryBuilder->getQuery()->getSingleResult();
		$sMaxId = '0';
		if (isset($aResult['m'])) {
			$sMaxId = $aResult['m'];
		}
		$nMaxId = (int) $sMaxId;
		return ($nMaxId + 1);
	}

	/**
	 * Метод добавления продукта
	 * (так как id в таблице строковый, приходится вычислять слдедующий свободный и использовать транзакции)
	 * @param string $sTitle
	 * @param string $sCode
	 * @return string id продукта
	*/
	public function add(string $sTitle, string $sCode) : string
	{
		$this->entityManager->beginTransaction();
		$nId = $this->getNextId();
		$oProduct = new Product(strval($nId), $sTitle, $sCode);
		$this->save($oProduct);
		$this->entityManager->commit();
		return $oProduct->getId();
	}

    public function save(Product $product): void
    {
        $persistenceObject = new ProductPersistenceObject(
            $product->getId(),
            $product->getTitle(),
            $product->getCode()
        );

        $this->entityManager->persist($persistenceObject);
        $this->entityManager->flush();
    }
}
